package com.jgil.dev.courtpicker.data

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

/**
 * Created by Johnny Gil Mejia on 2019-06-15.
 */
data class CourtModel(
    val courtName: String,
    val courtId: String,
    val courtPicture: String,
    val courtDescription: String
)

open class CourtSchedule (
    @PrimaryKey var codeSchedule: String? = null,
    @Index
    var courtId: String? = null,
    var date: String? = null,
    var user: String? = null,
    var rainProbability: String? = null,
    var courtName: String? = null
) : RealmObject() {


}



