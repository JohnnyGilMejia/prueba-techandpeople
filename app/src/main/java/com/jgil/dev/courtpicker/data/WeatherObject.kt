package com.jgil.dev.courtpicker.data
import com.google.gson.annotations.SerializedName


/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
data class WeatherObject(
    @SerializedName("currently")
    var currently: Currently,
    @SerializedName("latitude")
    var latitude: Double,
    @SerializedName("longitude")
    var longitude: Double,
    @SerializedName("offset")
    var offset: Int,
    @SerializedName("timezone")
    var timezone: String
)


data class Currently(
    @SerializedName("apparentTemperature")
    var apparentTemperature: Double,
    @SerializedName("cloudCover")
    var cloudCover: Double,
    @SerializedName("dewPoint")
    var dewPoint: Double,
    @SerializedName("humidity")
    var humidity: Double,
    @SerializedName("icon")
    var icon: String,
    @SerializedName("nearestStormBearing")
    var nearestStormBearing: Int,
    @SerializedName("nearestStormDistance")
    var nearestStormDistance: Int,
    @SerializedName("ozone")
    var ozone: Double,
    @SerializedName("precipIntensity")
    var precipIntensity: Double,
    @SerializedName("precipProbability")
    var precipProbability: Double,
    @SerializedName("pressure")
    var pressure: Double,
    @SerializedName("summary")
    var summary: String,
    @SerializedName("temperature")
    var temperature: Double,
    @SerializedName("time")
    var time: Int,
    @SerializedName("uvIndex")
    var uvIndex: Double,
    @SerializedName("visibility")
    var visibility: Double,
    @SerializedName("windBearing")
    var windBearing: Double,
    @SerializedName("windGust")
    var windGust: Double,
    @SerializedName("windSpeed")
    var windSpeed: Double
)
