package com.jgil.dev.courtpicker.ui.ScheduleActivity.presenter

import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleMVPInteractor
import com.jgil.dev.courtpicker.ui.ScheduleActivity.view.ScheduleMVPView
import com.jgil.dev.courtpicker.ui.base.presenter.MVPPresenter

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface ScheduleMVPPresenter  <V : ScheduleMVPView, I : ScheduleMVPInteractor> : MVPPresenter<V, I> {
    fun createSchedule(user:String, rainProbability:String)
    fun isDateAvailable(date:String)
    fun closeRealm()
    fun setCourtModel(courtModel: CourtModel)
}