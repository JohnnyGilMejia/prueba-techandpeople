package com.jgil.dev.courtpicker.di.builder

import com.jgil.dev.courtpicker.ui.ScheduleActivity.ScheduleActivityModule
import com.jgil.dev.courtpicker.ui.ScheduleActivity.view.ScheduleActivity
import com.jgil.dev.courtpicker.ui.homeActivity.HomeActivityModule
import com.jgil.dev.courtpicker.ui.homeActivity.view.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
@Module
abstract class ActivityBuilder {


    @ContributesAndroidInjector(modules = [(ScheduleActivityModule::class)])
    abstract fun bindScheduleActivityModule(): ScheduleActivity


    @ContributesAndroidInjector(modules = [(HomeActivityModule::class)])
    abstract fun bindCreateHomeActivity(): HomeActivity


}