package com.jgil.dev.courtpicker.utils

import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.data.CourtSchedule

/**
 * Created by Johnny Gil Mejia on 2019-06-15.
 */
class Constants {

    companion object{

        val url = "https://api.darksky.net/"

        val urlCourtA = "https://images.unsplash.com/photo-1480180566821-a7d525cdfc5e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
        val urlCourtB = "https://images.unsplash.com/photo-1499771749117-dd6fcfcb5ea7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
        val urlCourtC = "https://images.unsplash.com/photo-1499510318569-1a3d67dc3976?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"

        var courtA = CourtModel("Court A","1","","")
        var courtB = CourtModel("Court B","2","","")
        var courtC = CourtModel("Court C","3","","")

        val courtsList = listOf(
            courtA,
            courtB,
            courtC
        )

        private var courtScheduleA = CourtSchedule("0","1","20190707","Johnny","18%","Court A")
        private var courtScheduleB = CourtSchedule("1","2","20190718","Fernado","7%","Court B")
        private var courtScheduleC = CourtSchedule("2","3","20190727","Julio","3%","Court C")

        val courtsScheduleList = listOf(
            courtScheduleA,
            courtScheduleB,
            courtScheduleC
        )

        val imageUrl = "https://cdn.pixabay.com/photo/2016/11/29/01/14/athletes-1866487_960_720.jpg"
    }
}