package com.jgil.dev.courtpicker.ui.homeActivity.view

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.base.view.MVPView

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
interface HomeMVPView : MVPView {
    fun loadList(listCourtSchedule: List<CourtSchedule>)
    fun showMessageAddCourt()
    fun deleteSuccess()
}