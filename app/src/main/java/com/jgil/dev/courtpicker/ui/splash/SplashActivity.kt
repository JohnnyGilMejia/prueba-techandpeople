package com.jgil.dev.courtpicker.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jgil.dev.courtpicker.ui.homeActivity.view.HomeActivity

/**
 * Created by Johnny Gil Mejia on 2019-06-15.
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.activity_login)
        launchActivity()

    }

    private fun launchActivity(){
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
    }
}