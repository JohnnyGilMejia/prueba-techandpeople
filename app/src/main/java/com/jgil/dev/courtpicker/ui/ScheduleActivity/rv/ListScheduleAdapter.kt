package com.jgil.dev.courtpicker.ui.ScheduleActivity.rv

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.ui.base.BaseInterfaces

/**
 * Created by Johnny Gil Mejia on 2019-06-15.
 */
class ListScheduleAdapter (private val list: List<CourtModel>, private val callback:BaseInterfaces.baseInterfacesAdapters?)
    : RecyclerView.Adapter<ScheduleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ScheduleViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        val courtModel: CourtModel = list[position]
        holder.bind(courtModel)
        holder.btnAgenderAhora?.setOnClickListener {
            callback?.itemOnClic(position,courtModel)
        }
    }

    override fun getItemCount(): Int = list.size

}