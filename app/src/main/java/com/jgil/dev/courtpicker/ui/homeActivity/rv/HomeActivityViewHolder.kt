package com.jgil.dev.courtpicker.ui.homeActivity.rv

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jgil.dev.courtpicker.R
import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.utils.Constants

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class HomeActivityViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.layout_rv_home_activity, parent, false)) {
    private var tvCourtName: TextView? = null
    private var imgCourt: ImageView
    private var tvUser: TextView? = null
    private var tvRainProbability: TextView? = null
    private var tvDate: TextView? = null
    var context: Context
    var btnCancelar: Button? = null


    init {
        tvCourtName = itemView.findViewById(R.id.tvCourtName)
        btnCancelar = itemView.findViewById(R.id.btnCancelar)
        imgCourt = itemView.findViewById(R.id.imgCourt)
        tvUser = itemView.findViewById(R.id.tvUser)
        tvRainProbability = itemView.findViewById(R.id.tvRainProbability)
        tvDate = itemView.findViewById(R.id.tvDateDisponibility)
        context = parent.context
    }

    @SuppressLint("SetTextI18n")
    fun bind(courtSchedule: CourtSchedule) {
        tvCourtName?.text = courtSchedule.courtName
        tvDate?.text = courtSchedule.date.toString()
        tvUser?.text = courtSchedule.user
        tvRainProbability?.text = "Probabilidad de lluvia ${courtSchedule.rainProbability}%"
        when (courtSchedule.courtId) {
            "1" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtA)
                        .into(imgCourt)
                }
            }
            "2" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtB)
                        .into(imgCourt)
                }
            }
            "3" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtC)
                        .into(imgCourt)
                }
            }
        }
    }
}

