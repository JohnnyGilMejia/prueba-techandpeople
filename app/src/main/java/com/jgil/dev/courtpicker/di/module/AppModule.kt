package com.jgil.dev.courtpicker.di.module

import android.app.Application
import android.content.Context
import com.jgil.dev.courtpicker.data.bd.RealmServices
import dagger.Module
import dagger.Provides
import io.realm.Realm
import javax.inject.Singleton

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application


    @Provides
    fun provideRealm(): Realm {
        return Realm.getDefaultInstance()
    }

    @Provides
    fun provideRealmService(realm: Realm): RealmServices {
        return RealmServices(realm)
    }
}