package com.jgil.dev.courtpicker.ui.ScheduleActivity.view

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jgil.dev.courtpicker.R
import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleMVPInteractor
import com.jgil.dev.courtpicker.ui.ScheduleActivity.presenter.ScheduleMVPPresenter
import com.jgil.dev.courtpicker.ui.ScheduleActivity.rv.ListScheduleAdapter
import com.jgil.dev.courtpicker.ui.base.view.BaseActivity
import com.jgil.dev.courtpicker.ui.base.BaseInterfaces
import com.jgil.dev.courtpicker.utils.Constants
import com.jgil.dev.courtpicker.utils.setTransparentStatusBar
import kotlinx.android.synthetic.main.activity_shedule_details.*
import java.util.*
import javax.inject.Inject


class ScheduleActivity : BaseActivity(), BaseInterfaces.baseInterfacesAdapters, ScheduleMVPView {



    private lateinit var myContext: Context
    private lateinit var interfaces: BaseInterfaces.baseInterfacesAdapters
    private lateinit var btnCancelar: Button
    private lateinit var btnAceptar: Button
    private lateinit var tvDateDisponibility:TextView
    private lateinit var edtDate:EditText
    private lateinit var edtName:EditText
    private lateinit var tvCourtSelected: TextView
    private lateinit var subParent: ConstraintLayout
    private lateinit var rvCourt:RecyclerView
    private lateinit var ivDate: ImageView

    @Inject
    internal lateinit var presenter: ScheduleMVPPresenter<ScheduleMVPView,ScheduleMVPInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        setTransparentStatusBar()

        presenter.onAttach(this)
        myContext = this
        interfaces = this
        setupSubComponentsIncludeLayout()
        rvApply()

        btnCancelar.setOnClickListener {
            showDetail(false)
            clearFields()
        }
        ivDate.setOnClickListener{clickDataPicker()}
        btnAceptar.setOnClickListener{presenter.createSchedule(edtName.text.toString(),tvRainProbability.text.toString())}

    }

    private fun setupSubComponentsIncludeLayout(){
        rvCourt = findViewById(R.id.rvCourt)
        btnCancelar = findViewById(R.id.btnCancelar)
        btnAceptar = findViewById(R.id.btnAceptar)
        tvDateDisponibility = findViewById(R.id.tvDateDisponibility)
        edtDate = findViewById(R.id.edtDate)
        edtName = findViewById(R.id.edtName)
        tvCourtSelected = findViewById(R.id.tvCourtSelected)
        subParent = findViewById(R.id.subParent)
        ivDate = findViewById(R.id.ivDate)
    }

    fun clickDataPicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in Toast
            var formattedDate = "$dayOfMonth/$month/$year"
            edtDate.setText(formattedDate)
            presenter.isDateAvailable(formattedDate)

        }, year, month, day)
        dpd.show()
    }

    private fun rvApply(){

        rvCourt.apply {
            layoutManager = LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL,false)
            adapter = ListScheduleAdapter(Constants.courtsList,interfaces)
        }
    }

    override fun itemOnClic(pos: Int, obj: Any) {
        val court: CourtModel = obj as CourtModel
        presenter.setCourtModel(court)
        showDetail(true)
      //  Toast.makeText(this,"Clic on court: ${court.courtName}",Toast.LENGTH_SHORT).show()
    }



    fun showDetail(show:Boolean)
    {
        if(show){
            subParent.visibility = View.VISIBLE
            rvCourt.visibility = View.GONE
        }else
        {
            subParent.visibility = View.GONE
            rvCourt.visibility = View.VISIBLE
        }
    }

    override fun onSuccesfullScheduleCreated() {
        showDetail(false)
        Toast.makeText(myContext,resources.getString(R.string.court_create_succesfull),Toast.LENGTH_LONG).show()
    }

    override fun clearFields() {
        edtDate.setText("")
        edtName.setText("")
        tvRainProbability.setText("")
        tvDateDisponibility.visibility = View.INVISIBLE
    }

    override fun onErrorScheduleCreated() {
    }

    override fun onDateUnavailable() { tvDateDisponibility.visibility = View.VISIBLE }

    override fun onDateAvailable() { tvDateDisponibility.visibility = View.INVISIBLE }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun onDestroy() {
        presenter.closeRealm()
        presenter.onDetach()
        super.onDestroy()
    }

    override fun onUserError() { edtName.error = "" }

    override fun onDateError() { edtDate.error = ""}

    override fun setCourtLabelName(name:String) {
        tvCourtSelected.text = name
    }

    override fun onCantCreateScheduleDateIsNotAvailable() {
        Toast.makeText(myContext,resources.getString(R.string.cant_create_on_this_date),Toast.LENGTH_LONG).show()
    }

    @SuppressLint("SetTextI18n")
    override fun onSetRainProbability(probabilidad: String?) {
        probabilidad?.let {
            tvRainProbability.text = probabilidad
        }
    }
}

