package com.jgil.dev.courtpicker.ui.base.presenter

import com.jgil.dev.courtpicker.ui.base.interator.MVPInteractor
import com.jgil.dev.courtpicker.ui.base.view.MVPView

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
abstract class BasePresenter<V : MVPView, I : MVPInteractor> internal constructor(protected var interactor: I?) : MVPPresenter<V, I> {

    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null

    override fun onAttach(view: V?) {
        this.view = view
    }

    override fun getView(): V? = view

    override fun onDetach() {
        view = null
        interactor = null
    }

}