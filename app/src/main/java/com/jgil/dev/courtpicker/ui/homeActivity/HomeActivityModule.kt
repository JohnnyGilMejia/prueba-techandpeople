package com.jgil.dev.courtpicker.ui.homeActivity

import com.jgil.dev.courtpicker.ui.homeActivity.interator.HomeInterator
import com.jgil.dev.courtpicker.ui.homeActivity.interator.HomeMVPInterator
import com.jgil.dev.courtpicker.ui.homeActivity.presenter.HomeMVPPresenter
import com.jgil.dev.courtpicker.ui.homeActivity.presenter.HomePresenter
import com.jgil.dev.courtpicker.ui.homeActivity.view.HomeMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(interactor: HomeInterator): HomeMVPInterator = interactor

    @Provides
    internal fun provideLoginPresenter(presenter: HomePresenter<HomeMVPView, HomeMVPInterator>)
            : HomeMVPPresenter<HomeMVPView, HomeMVPInterator> = presenter
}