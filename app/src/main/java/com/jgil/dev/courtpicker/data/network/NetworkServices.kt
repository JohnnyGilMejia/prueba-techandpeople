package com.jgil.dev.courtpicker.data.network

import com.jgil.dev.courtpicker.data.WeatherObject
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface NetworkServices {


    /*
    Para fines de simplicidad ponga fijas las coordenadas de Santo Domingo
     */
    @GET("forecast/a1bfea853b33380f88b2eeb2d7f2524a/18.4855,-69.8731")
    fun getForecast(): Call<WeatherObject>
}