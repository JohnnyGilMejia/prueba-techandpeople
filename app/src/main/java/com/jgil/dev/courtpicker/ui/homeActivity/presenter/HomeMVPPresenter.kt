package com.jgil.dev.courtpicker.ui.homeActivity.presenter

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.base.presenter.MVPPresenter
import com.jgil.dev.courtpicker.ui.homeActivity.interator.HomeMVPInterator
import com.jgil.dev.courtpicker.ui.homeActivity.view.HomeMVPView

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
interface HomeMVPPresenter  <V : HomeMVPView, I : HomeMVPInterator> : MVPPresenter<V, I> {

    fun searchCourtsSchedules()
    fun deleteCourtSchedule(position: Int,code:String)
    fun searchRainProbability()
    fun closeRealm()
}