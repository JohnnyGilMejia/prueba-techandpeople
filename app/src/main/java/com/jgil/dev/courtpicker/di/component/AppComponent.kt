package com.jgil.dev.courtpicker.di.component

import android.app.Application
import com.jgil.dev.courtpicker.CourtPicker
import com.jgil.dev.courtpicker.di.builder.ActivityBuilder
import com.jgil.dev.courtpicker.di.module.AppModule
import com.jgil.dev.courtpicker.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
@Singleton
@Component(modules = [
    (AndroidInjectionModule::class),
    (AppModule::class), (ActivityBuilder::class),
    (NetworkModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

    fun inject(app: CourtPicker)

}