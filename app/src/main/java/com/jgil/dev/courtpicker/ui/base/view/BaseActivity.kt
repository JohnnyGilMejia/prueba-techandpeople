package com.jgil.dev.courtpicker.ui.base.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.jgil.dev.courtpicker.utils.setTransparentStatusBar
import dagger.android.AndroidInjection

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
abstract class BaseActivity: AppCompatActivity(), MVPView{

    override fun onCreate(savedInstanceState: Bundle?) {
        performDI()
        super.onCreate(savedInstanceState)
    }
    private fun performDI() = AndroidInjection.inject(this)


}