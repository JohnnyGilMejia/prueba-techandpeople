package com.jgil.dev.courtpicker

import android.app.Activity
import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.jgil.dev.courtpicker.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class CourtPicker : Application(), HasActivityInjector {


    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    companion object {
        lateinit var instance : CourtPicker
            private set

        fun hasNetwork(): Boolean {
            return instance.checkIfHasNetwork()
        }

        fun getInstanceCourtPicker() : CourtPicker {
            return instance
        }

    }


    override fun onCreate() {
        super.onCreate()
        instance = this

        Realm.init(this)
        Realm.setDefaultConfiguration(
            RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build())

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
    }

    fun checkIfHasNetwork(): Boolean {
        val cm = getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun activityInjector() = activityDispatchingAndroidInjector

}