package com.jgil.dev.courtpicker.ui.ScheduleActivity.interator

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.interator.MVPInteractor

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface ScheduleMVPInteractor : MVPInteractor {
    fun getWeatherForDate(date:String,callback: ApiResponse.apiResponseScheduleCourt)
}