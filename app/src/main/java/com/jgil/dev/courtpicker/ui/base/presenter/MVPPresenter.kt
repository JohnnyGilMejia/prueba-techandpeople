package com.jgil.dev.courtpicker.ui.base.presenter

import com.jgil.dev.courtpicker.ui.base.interator.MVPInteractor
import com.jgil.dev.courtpicker.ui.base.view.MVPView

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}