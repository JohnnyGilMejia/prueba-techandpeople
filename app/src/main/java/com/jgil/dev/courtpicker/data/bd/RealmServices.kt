package com.jgil.dev.courtpicker.data.bd

import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.data.CourtSchedule
import io.realm.Realm
import io.realm.RealmResults
import io.realm.kotlin.where

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class RealmServices(realm: Realm) {

    private var mRealm: Realm = realm


    fun closeRealm() {
        mRealm.close()
    }

    fun getAllCourtScheduled() : RealmResults<CourtSchedule> = mRealm.where<CourtSchedule>().sort("date")
        .findAll()

    fun getAllCourtScheduledForDate(date:String) : RealmResults<CourtSchedule> = mRealm.where<CourtSchedule>()
        .equalTo("date",date).findAll()



    fun copyCourtScheduleToRealm(courtSchedule: CourtSchedule){
        mRealm.beginTransaction()
        mRealm.copyToRealm(courtSchedule)
        mRealm.commitTransaction()
    }

    fun deleteCourtScheduled(code:String){
        var resul: RealmResults<CourtSchedule> = mRealm.where<CourtSchedule>()
            .equalTo("codeSchedule",code).findAll()
        mRealm.beginTransaction()
        resul.deleteAllFromRealm()
        mRealm.commitTransaction()
    }

    fun setWeatherProbability(weatherProbability: String?) : RealmResults<CourtSchedule> {

        var result = mRealm.where<CourtSchedule>().sort("date")
            .findAll()
        mRealm.beginTransaction()
        result.forEach {
            it.rainProbability = weatherProbability ?: "0"
        }
        mRealm.commitTransaction()

        return result
    }
}