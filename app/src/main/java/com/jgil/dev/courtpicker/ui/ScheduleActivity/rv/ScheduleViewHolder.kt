package com.jgil.dev.courtpicker.ui.ScheduleActivity.rv

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jgil.dev.courtpicker.R
import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.utils.Constants

/**
 * Created by Johnny Gil Mejia on 2019-06-15.
 */
class ScheduleViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.layout_rv_court_picker, parent, false)) {
    private var tvCourtName: TextView? = null
    var btnAgenderAhora: Button? = null
    private var imgCourt: ImageView
    var context:Context


    init {
        tvCourtName = itemView.findViewById(R.id.tvCourtName)
        btnAgenderAhora = itemView.findViewById(R.id.btnAgenderAhora)
        imgCourt = itemView.findViewById(R.id.imgCourt)
        context = parent.context
    }

    fun bind(courtModel: CourtModel) {
        tvCourtName?.text = courtModel.courtName
        when(courtModel.courtId)
        {
            "1" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtA)
                        .into(imgCourt)
                }
            }
            "2" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtB)
                        .into(imgCourt)
                }
            }
            "3" -> {
                imgCourt.let {
                    Glide.with(context)
                        .load(Constants.urlCourtC)
                        .into(imgCourt)
                }
            }
        }
    }

}