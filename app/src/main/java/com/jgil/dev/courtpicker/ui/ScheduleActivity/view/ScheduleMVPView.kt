package com.jgil.dev.courtpicker.ui.ScheduleActivity.view

import com.jgil.dev.courtpicker.ui.base.view.MVPView

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface ScheduleMVPView : MVPView {

    fun onSuccesfullScheduleCreated()
    fun onErrorScheduleCreated()
    fun onDateUnavailable()
    fun onDateAvailable()
    fun clearFields()
    fun onUserError()
    fun onDateError()
    fun setCourtLabelName(name:String)
    fun onCantCreateScheduleDateIsNotAvailable()
    fun onSetRainProbability(probabilidad:String?)
}