package com.jgil.dev.courtpicker.ui.homeActivity.rv

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.base.BaseInterfaces

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class HomeActivityAdapter  (private val list: List<CourtSchedule>,private val callback: BaseInterfaces.baseInterfacesAdapters?)
    : RecyclerView.Adapter<HomeActivityViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeActivityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return HomeActivityViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: HomeActivityViewHolder, position: Int) {
        val courtModel: CourtSchedule = list[position]
        holder.bind(courtModel)
        holder.btnCancelar?.setOnClickListener {
            callback?.itemOnClic(position,courtModel)
        }
    }

    override fun getItemCount(): Int = list.size

}