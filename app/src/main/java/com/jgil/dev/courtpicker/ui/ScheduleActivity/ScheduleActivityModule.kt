package com.jgil.dev.courtpicker.ui.ScheduleActivity

import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleInteractor
import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleMVPInteractor
import com.jgil.dev.courtpicker.ui.ScheduleActivity.presenter.ScheduleMVPPresenter
import com.jgil.dev.courtpicker.ui.ScheduleActivity.presenter.SchedulePresenter
import com.jgil.dev.courtpicker.ui.ScheduleActivity.view.ScheduleMVPView
import dagger.Module
import dagger.Provides

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */

@Module
class ScheduleActivityModule {

    @Provides
    internal fun provideLoginInteractor(interactor: ScheduleInteractor): ScheduleMVPInteractor = interactor

    @Provides
    internal fun provideLoginPresenter(presenter: SchedulePresenter<ScheduleMVPView, ScheduleMVPInteractor>)
            : ScheduleMVPPresenter<ScheduleMVPView, ScheduleMVPInteractor> = presenter

}