package com.jgil.dev.courtpicker.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jgil.dev.courtpicker.BuildConfig
import com.jgil.dev.courtpicker.CourtPicker
import com.jgil.dev.courtpicker.data.network.LogJsonInterceptor
import com.jgil.dev.courtpicker.data.network.NetworkServices
import com.jgil.dev.courtpicker.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
@Module
class NetworkModule {


        @Singleton
        @Provides
        open fun provideRetrofit2(baseUrl: String): Retrofit {

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(LogJsonInterceptor())

            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(provideGsonSetLetient()))
                .client(provideLoggingCapableHttpClient())
                .build()
        }

        @Singleton
        @Provides
        fun provideGsonSetLetient(): Gson {

            return GsonBuilder()
                .setLenient()
                .create()
        }

        @Singleton
        @Provides
        fun provideLoggingCapableHttpClient(): OkHttpClient {


            return OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(provideHttpLoginInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .build()
        }

        @Singleton
        @Provides
        fun provideCache(): Cache? {
            var cache: Cache? = null
            try {
                cache = Cache(
                    File(CourtPicker.getInstanceCourtPicker().getCacheDir(), "http-cache"),
                    (10 * 1024 * 1024).toLong()
                ) // 10 MB
            } catch (e: Exception) {
                Timber.e(e, "Could not create Cache!")
            }

            return cache
        }

        @Singleton
        @Provides
        fun provideCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                val response = chain.proceed(chain.request())

                // re-write response header to force use of cache
                val cacheControl = CacheControl.Builder()
                    .maxAge(2, TimeUnit.MINUTES)
                    .build()

                response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build()
            }
        }

        @Singleton
        @Provides
        fun provideOfflineCacheInterceptor(): Interceptor {
            return Interceptor { chain ->
                var request = chain.request()

                if (!CourtPicker.hasNetwork()) {
                    val cacheControl = CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build()

                    request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build()
                }

                chain.proceed(request)
            }
        }

        @Singleton
        @Provides
        fun provideHttpLoginInterceptor(): HttpLoggingInterceptor {
            val loggin = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Timber.d(message) })
            loggin.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            return loggin
        }

        @Provides
        open fun provideCreateUserService(): NetworkServices {
            return provideRetrofit2(Constants.url)
                .create<NetworkServices>(NetworkServices::class.java)
        }




}
