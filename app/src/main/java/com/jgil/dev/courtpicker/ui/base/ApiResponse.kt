package com.jgil.dev.courtpicker.ui.base

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.data.WeatherObject

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
interface ApiResponse {
    fun onSuccess(weather:WeatherObject?)
    fun onError()
    interface apiResponsePlusCourts{
        fun onSUccess(listCourts: List<CourtSchedule>,rainProbability: String?)
        fun onError(error:String,listCourts : List<CourtSchedule>)
    }
    interface apiResponseScheduleCourt{
        fun onSUccess(rainProbability: String?)
        fun onError(error:String)
    }
}