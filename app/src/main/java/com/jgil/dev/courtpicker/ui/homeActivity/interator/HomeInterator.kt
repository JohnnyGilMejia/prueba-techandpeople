package com.jgil.dev.courtpicker.ui.homeActivity.interator

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.data.network.NetworkServices
import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleMVPInteractor
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.interator.BaseInteractor
import com.jgil.dev.courtpicker.ui.base.interator.MVPInteractor
import com.jgil.dev.courtpicker.utils.enqueue
import javax.inject.Inject

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
class HomeInterator @Inject internal constructor() : BaseInteractor(), HomeMVPInterator {


    @Inject
    lateinit var networkServices: NetworkServices

    override fun getWeather(callback: ApiResponse.apiResponsePlusCourts, listCourts: List<CourtSchedule>) {
        networkServices.getForecast().enqueue{
            onResponse = {
                if(it.isSuccessful){
                    var weather:Double = it.body()?.currently?.precipProbability ?: 0.0
                    callback.onSUccess(listCourts,(weather*100.0).toString())
                }else
                    callback.onError(it.errorBody().toString(),listCourts)
            }
            onFailure = {
                callback.onError(it?.message.toString(),listCourts)
            }
        }
    }
}