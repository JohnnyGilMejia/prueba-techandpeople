package com.jgil.dev.courtpicker.ui.ScheduleActivity.interator

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.data.bd.RealmServices
import com.jgil.dev.courtpicker.data.network.NetworkServices
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.interator.BaseInteractor
import com.jgil.dev.courtpicker.utils.enqueue
import javax.inject.Inject

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class ScheduleInteractor @Inject internal constructor() : BaseInteractor(),ScheduleMVPInteractor{


    @Inject
    lateinit var networkServices: NetworkServices


    override fun getWeatherForDate(date: String, callback: ApiResponse.apiResponseScheduleCourt) {
        networkServices.getForecast().enqueue{
            onResponse = {
                if(it.isSuccessful){
                    var weather:Double = it.body()?.currently?.precipProbability ?: 0.0
                    callback.onSUccess((weather*100.0).toString())
                }else
                    callback.onError(it.errorBody().toString())
            }
            onFailure = {
                callback.onError(it?.message.toString())
            }
        }
    }




}