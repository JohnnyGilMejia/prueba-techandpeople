package com.jgil.dev.courtpicker.ui.ScheduleActivity.presenter

import android.util.Log
import com.jgil.dev.courtpicker.data.CourtModel
import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.data.bd.RealmServices
import com.jgil.dev.courtpicker.ui.ScheduleActivity.interator.ScheduleMVPInteractor
import com.jgil.dev.courtpicker.ui.ScheduleActivity.view.ScheduleMVPView
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.presenter.BasePresenter
import javax.inject.Inject
import com.jgil.dev.courtpicker.utils.getRandomString


/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
class SchedulePresenter <V : ScheduleMVPView, I : ScheduleMVPInteractor> @Inject internal constructor(interactor: I) : BasePresenter<V, I>(interactor = interactor),
    ScheduleMVPPresenter<V, I>, ApiResponse.apiResponseScheduleCourt{



    @Inject lateinit var realmServices: RealmServices
    private lateinit var courtModel: CourtModel
    private  var date:String? = null
    private var dateAvailable: Boolean = true

    override fun createSchedule(user: String, rainProbability: String) {

        if(user.isNullOrEmpty()){
            getView()?.onUserError()
        }else if( date.isNullOrBlank())
        {
            getView()?.onDateError()
        }else if(!dateAvailable){
            getView()?.onCantCreateScheduleDateIsNotAvailable()
        }else
        {
            var courtSchedule = CourtSchedule(getRandomString(6),courtModel.courtId,date,user,rainProbability,courtModel.courtName)

            var result = realmServices.copyCourtScheduleToRealm(courtSchedule)


            getView()?.onSuccesfullScheduleCreated()
            getView()?.clearFields()
        }
    }

    override fun isDateAvailable(date: String) {

        this.date = date

        var result = realmServices.getAllCourtScheduledForDate(date)

        var filterdList =  result.filter { it.courtId == courtModel.courtId }

        if(filterdList.size < 3){
            dateAvailable = true
            getView()?.onDateAvailable()
            interactor?.getWeatherForDate(date,this)
        }
        else{
            dateAvailable = false
            getView()?.onDateUnavailable()
        }
    }

    override fun closeRealm() {
        realmServices.closeRealm()
    }

    override fun setCourtModel(courtModel: CourtModel) {
        this.courtModel = courtModel

        getView()?.setCourtLabelName(courtModel.courtName)
    }

    override fun onSUccess(rainProbability: String?) {
        getView()?.onSetRainProbability(rainProbability)
    }

    override fun onError(error: String) {

    }

}