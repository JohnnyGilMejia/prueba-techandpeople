package com.jgil.dev.courtpicker.ui.homeActivity.presenter

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.data.bd.RealmServices
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.presenter.BasePresenter
import com.jgil.dev.courtpicker.ui.homeActivity.interator.HomeMVPInterator
import com.jgil.dev.courtpicker.ui.homeActivity.view.HomeMVPView
import javax.inject.Inject

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
class HomePresenter <V : HomeMVPView, I : HomeMVPInterator> @Inject internal constructor(interactor: I) : BasePresenter<V, I>(interactor = interactor),
    HomeMVPPresenter<V, I>, ApiResponse.apiResponsePlusCourts {

    @Inject lateinit var realmServices: RealmServices

    override fun deleteCourtSchedule(pos:Int,code: String) {
        realmServices.deleteCourtScheduled(code)
        searchCourtsSchedules()
        getView()?.deleteSuccess()
    }

    override fun searchRainProbability() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun searchCourtsSchedules() {
        var result = realmServices.getAllCourtScheduled()
        if(result.isNotEmpty()) {
            getView()?.showProgress()
            interactor?.getWeather(this,result)
        }else {
            getView()?.showMessageAddCourt()
            getView()?.hideProgress()
        }

    }

    override fun closeRealm() {
        realmServices.closeRealm()
    }

    override fun onSUccess(listCourts: List<CourtSchedule>,rainProbability: String?) {
        var result = realmServices.setWeatherProbability(rainProbability)
        getView()?.loadList(result)
        getView()?.hideProgress()
    }

    override fun onError(error: String, listCourts: List<CourtSchedule>) {
        getView()?.loadList(listCourts)
        getView()?.hideProgress()
    }


}