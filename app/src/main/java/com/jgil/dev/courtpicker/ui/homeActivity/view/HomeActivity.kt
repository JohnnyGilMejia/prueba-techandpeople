package com.jgil.dev.courtpicker.ui.homeActivity.view

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.jgil.dev.courtpicker.R
import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.ScheduleActivity.view.ScheduleActivity
import com.jgil.dev.courtpicker.ui.base.BaseInterfaces
import com.jgil.dev.courtpicker.ui.base.view.BaseActivity
import com.jgil.dev.courtpicker.ui.homeActivity.interator.HomeMVPInterator
import com.jgil.dev.courtpicker.ui.homeActivity.presenter.HomeMVPPresenter
import com.jgil.dev.courtpicker.ui.homeActivity.rv.HomeActivityAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.content_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity(), HomeMVPView, BaseInterfaces.baseInterfacesAdapters {

    @Inject
    internal lateinit var presenter: HomeMVPPresenter<HomeMVPView,HomeMVPInterator>
    private lateinit var interfaces: BaseInterfaces.baseInterfacesAdapters

    val positiveButtonClick = { dialog: DialogInterface, which: Int -> }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.onAttach(this)
        interfaces = this
        presenter.searchCourtsSchedules()

        fab.setOnClickListener { view ->
            launchActivity()
        }
    }

    private fun launchActivity(){
        val intent = Intent(this, ScheduleActivity::class.java)
        startActivity(intent)
    }

    override fun loadList(listCourtSchedule: List<CourtSchedule>) {
        rvHome?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = HomeActivityAdapter(listCourtSchedule,interfaces)
        }
        tvHomeMessage.visibility = View.INVISIBLE
        rvHome.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        presenter?.searchCourtsSchedules()
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun onDestroy() {
        presenter.closeRealm()
        presenter.onDetach()
        super.onDestroy()
    }

    override fun showMessageAddCourt() {
        tvHomeMessage.visibility = View.VISIBLE
        rvHome.visibility = View.INVISIBLE
    }

    override fun itemOnClic(pos: Int, obj: Any) {
        val court:CourtSchedule = obj as CourtSchedule
        court.let {
            it.codeSchedule?.let {
                alertWantToDelete(pos,it,tvHomeMessage)
               //presenter.deleteCourtSchedule(pos,it)
            }
        }
    }

    override fun deleteSuccess() {
        alertDeletedSucces(rvHome)
    }

    fun alertDeletedSucces(view: View){

        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle(context.resources.getString(R.string.dialog_title_deleted_item))
            setMessage(context.resources.getString(R.string.dialog_message_deleted_item))
            setPositiveButton("OK", DialogInterface.OnClickListener(function = positiveButtonClick))
            show()
        }
    }

    fun alertWantToDelete(pos:Int, code:String, view: View){

        val builder = AlertDialog.Builder(this)

        with(builder)
        {
            setTitle(context.resources.getString(R.string.dialog_title_attention))
            setMessage(context.resources.getString(R.string.dialog_message_attention))
            setPositiveButton(android.R.string.yes) { _,_ ->
                presenter.deleteCourtSchedule(pos,code)

            }
            setNegativeButton(android.R.string.cancel) { _,_ ->

            }
            show()
        }

    }

}
