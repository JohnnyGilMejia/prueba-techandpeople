package com.jgil.dev.courtpicker.ui.homeActivity.interator

import com.jgil.dev.courtpicker.data.CourtSchedule
import com.jgil.dev.courtpicker.ui.base.ApiResponse
import com.jgil.dev.courtpicker.ui.base.interator.MVPInteractor

/**
 * Created by Johnny Gil Mejia on 2019-06-17.
 */
interface HomeMVPInterator : MVPInteractor {

    fun getWeather(callback: ApiResponse.apiResponsePlusCourts,listCourts : List<CourtSchedule>)
}