package com.jgil.dev.courtpicker.ui.base.view

/**
 * Created by Johnny Gil Mejia on 2019-06-16.
 */
interface MVPView {

    fun showProgress()

    fun hideProgress()

}